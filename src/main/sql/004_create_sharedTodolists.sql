
create table sharedTodolist (
	id serial,
	target integer REFERENCES users (id),
	list integer REFERENCES todo_lists (id),
	canCheck boolean not null DEFAULT false,
	canCreateItems boolean not null DEFAULT false,
	canShare boolean not null DEFAULT false,
	isAdmin boolean not null DEFAULT false,
	primary key(id),
	UNIQUE (target,list)

	);

update schema_info set version = 3;
