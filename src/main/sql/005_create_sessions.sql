
create table sessions (
    session_id varchar(50) not null,
    user_id int not null references users unique,
    remote_ip_address varchar(50) not null,
    created_at timestamp not null,
    updated_at timestamp not null,
    primary key(session_id)
); 

update schema_info set version = 4;

