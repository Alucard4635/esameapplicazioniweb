
create table users (
  id serial,
  username varchar(255) not null,
  email varchar(255) not null unique,
  encryptedPassword varchar(255) not null,
  saltForEncryption varchar(255) not null,
  primary key(id)
);

update schema_info set version = 3;

