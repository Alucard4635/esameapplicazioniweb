package it.xpug.todolists.repository;

import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Random;
import it.xpug.todolists.main.TodoListSession;
import it.xpug.todolists.main.User;
import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.ListOfRows;

public class DatabaseSessionRepository implements SessionRepository {

	private Database db;
	private Random seed;
	private UserRepository users;

	public DatabaseSessionRepository(Database db, UserRepository users) {
		this(db,users, new SecureRandom());
	}
	
	public DatabaseSessionRepository(Database db, UserRepository users, Random seed) {
		this.db = db;
		this.users = users;
		this.seed = seed;
		
	}
	
	@Override
	public String newSessionId() {
		return Long.toHexString(Math.abs(seed.nextLong()));
	}

	@Override
	public void add(TodoListSession session) {
		String sql="INSERT INTO sessions(session_id,user_id,remote_ip_address,created_at,updated_at)VALUES (?,?,?,?,?)";
		Timestamp creation = new Timestamp(session.getCreationDate().getTimeInMillis());
		Timestamp update = new Timestamp(session.getUpdateDate().getTimeInMillis());

		db.execute(sql, session.getId(),session.getUser().getId(),
				session.getIpAddress(),creation,update);
		
	}

	@Override
	public TodoListSession get(String sessionId) {
		String sql="SELECT * FROM sessions WHERE session_id=?";
		ListOfRows rows =db.select(sql, sessionId);
		if (rows.size()==0) {
			return null;
		}
		return generateASessionFromRow(rows.get(0));
	}

	private TodoListSession generateASessionFromRow(Map<String, Object> map) {
		GregorianCalendar creation = new GregorianCalendar();
		creation.setTimeInMillis(((Timestamp) map.get("created_at")).getTime());
		GregorianCalendar update = new GregorianCalendar();
		update.setTimeInMillis(((Timestamp) map.get("updated_at")).getTime());
		TodoListSession session=new TodoListSession(
				(String)map.get("session_id"),
				users.get((int) map.get("user_id")),
				(String)map.get("remote_ip_address"),
				creation,
				update);
		return session;
	}
	
	public void clear() {
		db.execute("truncate table sessions cascade");
	}

	public long size() {
		return (long) db.selectOneValue("select count(*) from sessions");
	}

	@Override
	public void clear(User userFound) {
		String sql="DELETE  FROM sessions WHERE user_id=?";
		db.execute(sql, userFound.getId());
	}


}
