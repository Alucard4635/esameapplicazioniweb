package it.xpug.todolists.repository;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.xpug.todolists.main.SharedPrivileges;
import it.xpug.todolists.main.SharedPrivileges.PrivilegesType;
import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.User;
import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.ListOfRows;

public class DatabaseShareTodolistRepository implements ShareTodolistRepository {
private Database db;
private DatabaseTodoListRepository todolistsDB;

	public DatabaseShareTodolistRepository(Database db,Database listsDB, UserRepository users) {
		this.db = db;
		this.todolistsDB = new DatabaseTodoListRepository(listsDB, users);
	}
	public DatabaseShareTodolistRepository(Database db, UserRepository users) {
		this.db = db;
		this.todolistsDB = new DatabaseTodoListRepository(db, users);
	}

	public DatabaseShareTodolistRepository(Database database,
			DatabaseTodoListRepository listRepository) {
				db = database;
				todolistsDB = listRepository;
	}
	@Override
	public int shareList(TodoList todoL, User target) {
		String sql = "INSERT INTO sharedTodolist (target,list) VALUES (?,?) RETURNING id";
		ListOfRows rows = db.select(sql, target.getId(), todoL.getId());
		int id = (int) rows.get(0).get("id");
		return id;
	}	



	@Override
	public List<SharedPrivileges<Boolean>> getSharedPrivileges(int todolistID) {
		List<SharedPrivileges<Boolean>> result=new LinkedList<SharedPrivileges<Boolean>>();
		String sql = "SELECT * FROM users JOIN sharedTodolist ON (users.id=sharedTodolist.target)WHERE sharedTodolist.list=?";
		ListOfRows rows = db.select(sql, todolistID);
		result.addAll(generateSharedPrivilegesFromRows(rows));
		return result;
	}


	private List<? extends SharedPrivileges<Boolean>> generateSharedPrivilegesFromRows(
			ListOfRows rows) {
		List<SharedPrivileges<Boolean>> result=new LinkedList<SharedPrivileges<Boolean>>();
		for (Map<String,Object> row : rows) {
			result.add(generateSharedPrivilegesFromRow(row));
		}
		return result;
	}

	private SharedPrivileges<Boolean> generateSharedPrivilegesFromRow(Map<String, Object> row) {
		User target=DatabaseUserRepository.generateUserFromRow(row);
		Boolean canCheck=(boolean) row.get("cancheck");
		Boolean canCreateItems=(boolean) row.get("cancreateitems");
		Boolean canShare=(boolean) row.get("canshare");
		Boolean isAdmin=(boolean) row.get("isadmin");

		return new SharedPrivileges<Boolean>(target,isAdmin,canCheck,canCreateItems,canShare);
	}

	@Override
	public List<TodoList> getAllSharedWith(int targetID) {
		List<TodoList> result = new ArrayList<TodoList>();
		ListOfRows rows = db.select("SELECT * FROM todo_lists JOIN sharedTodolist ON (todo_lists.id=sharedTodolist.list) WHERE target=?  ORDER BY todo_lists.id",targetID);
		result.addAll(todolistsDB.generateTodolistsFormRows(rows));
		return result;
	}
	


	@Override
	public long size() {
		return (long) db.selectOneValue("select count(*) from sharedTodolist");
	}
	@Override
	public void clear() {
		db.execute("truncate table sharedTodolist cascade");
	}

	@Override
	public void updatePrivileges(TodoList todolistWithPrivileges) {
		int idList=todolistWithPrivileges.getId();
		List<SharedPrivileges<Boolean>> privileges = todolistWithPrivileges.getPrivileges();
		for (SharedPrivileges<Boolean> sharedPrivileges : privileges) {
			updatePrivileges(idList, sharedPrivileges);
		}
	}
	@Override
	public void updatePrivileges(int idList,
			SharedPrivileges<Boolean> sharedPrivileges) {
		String sql = "UPDATE sharedTodolist SET canCheck = ? ,canCreateItems=?,isAdmin=?,canShare=? "
				+ "WHERE list = ? AND target=?";
		db.execute(sql, sharedPrivileges.getPrivilege(PrivilegesType.USE),
				sharedPrivileges.getPrivilege(PrivilegesType.UPDATE),
				sharedPrivileges.getPrivilege(PrivilegesType.ADMIN), 
				sharedPrivileges.getPrivilege(PrivilegesType.SHARE), 
				idList,
				sharedPrivileges.getTarget().getId());
	}

	@Override
	public void revokeShare(TodoList shared, User target) {
		String sql = "DELETE FROM sharedTodolist WHERE target=? AND list=? ";
		db.execute(sql, target.getId(), shared.getId());
	}
	@Override
	public boolean isSharedTo(TodoList todoList, User target) {
		String sql = "SELECT* FROM sharedTodolist WHERE target=? AND list=? ";
		ListOfRows result = db.select(sql, target.getId(), todoList.getId());
		if (result.size()>0) {
			return true;
		}
		return false;
	}


}
