package it.xpug.todolists.repository;

import it.xpug.todolists.main.TodoItem;
import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.User;
import it.xpug.todolists.main.User.UserFields;
import it.xpug.toolkit.db.*;

import java.sql.Timestamp;
import java.util.*;

public class DatabaseTodoListRepository implements TodoListRepository {

	private Database db;
	private UserRepository users;

	public DatabaseTodoListRepository(Database database,UserRepository users) {
		this.db = database;
		this.users = users;
	}

	@Override
	public TodoList get(int todoListId) {
		ListOfRows rows = db.select("select * from todo_lists where id = ?", todoListId);
		if (rows.size() == 0){
			return null;
		}
		
		
		Map<String, Object> firstRow = rows.get(0);
		TodoList todoList = getATodolistFromRow(firstRow);

		return todoList;
	}

	private TodoList getATodolistFromRow(Map<String, Object> row) {
		String name = (String) row.get("name");
		int ownerID= (int) row.get("owner");
		int id = (int) row.get("id");
		User owner = users.get(ownerID);
		TodoList todoList = new TodoList(id,owner, name);
		List<TodoItem> todoItems = getItems(todoList.getId());
		todoList.setItems(todoItems);
		
		return todoList;
	}

	public List<TodoItem> getItems(int todolistID) {
		List<TodoItem> todoItems=new ArrayList<TodoItem>();
		String sql = "select * from todo_items where todo_list_id = ?";
		for (Map<String, Object> row : db.select(sql, todolistID)) {
			TodoItem todoItem = generateTodoItemFromRow(row);
			todoItems.add(todoItem);
        }
		return todoItems;
	}

	public TodoItem generateTodoItemFromRow(Map<String, Object> row) {
		String text = (String) row.get("text");
		int todoItemId = (int) row.get("id");
		boolean isChecked = (boolean) row.get("checked");
		TodoItem todoItem = new TodoItem(todoItemId, text, isChecked);
		Timestamp timeCheck=(Timestamp) row.get("lastcheck");
		if (timeCheck!=null) {
			User who=users.get((int) row.get("whochecked"));
			todoItem.setTimeOfCheck(timeCheck);
			todoItem.setWhoCheck(who);
		}

		return todoItem;
	}

	@Override
	public int add(TodoList todoList) {
		String sql = "insert into todo_lists (name,owner) values (?,?) returning id";
		ListOfRows rows = db.select(sql, todoList.getName(), todoList.getOwnerID());
		int id = (int) rows.get(0).get("id");
		todoList.setId(id);
		return id;
	}

	@Override
	public List<? extends TodoList> getAllOwned(int ownerID) {
		List<TodoList> result = new ArrayList<TodoList>();
		ListOfRows rows = db.select("SELECT * FROM todo_lists WHERE owner=?  ORDER BY id",ownerID);
		result.addAll(generateTodolistsFormRows(rows));
		return result;
	}
	


	public List<? extends TodoList> generateTodolistsFormRows(ListOfRows rows) {
		List<TodoList> createdListFromRows = new ArrayList<TodoList>();
		for (Map<String, Object> row : rows) {
			createdListFromRows.add(getATodolistFromRow(row));
        }
		return createdListFromRows;
	}
	

	@Override
	public void clear() {
		db.execute("truncate todo_lists cascade");
	}

	@Override
	public long size() {
		return (long) db.selectOneValue("select count(*) from todo_lists");
	}

	@Override
    public void update(TodoList todoList) {
		int idList;
		idList = todoList.getId();
		Integer idItem;
		for (TodoItem todoItem : todoList.getItems()) {
			idItem = todoItem.getId();
			if (idItem == null) {
				String sql = "insert into todo_items (todo_list_id, text, checked)"
						+ "values (?, ?, ?)";
				db.execute(sql, idList, todoItem.getText(), todoItem.isChecked());
			}else {
				//else update the todoitem...
				String sql;
				if (todoItem.getTimeOfCheck()==null||todoItem.getWhoCheck()==null) {
					sql = "UPDATE todo_items SET checked = ?"
							+ "WHERE todo_list_id = ? AND id=?";
					db.execute(sql,  
							todoItem.isChecked(),
							idList,idItem);
				}else {
					sql = "UPDATE todo_items SET checked = ?, lastCheck=?,whoChecked=?"
							+ "WHERE todo_list_id = ? AND id=?";
					db.execute(sql,  
							todoItem.isChecked(),todoItem.getTimeOfCheck(),Integer.parseInt(todoItem.getWhoCheck().getField(UserFields.ID)),
							idList,idItem);
				}
			}
        }
		List<TodoItem> toRemove = todoList.getRemovedItems();
		for (TodoItem todoItem : toRemove) {
			idItem = todoItem.getId();
			System.out.println("removing:"+idItem);
			String sql = "DELETE FROM todo_items WHERE todo_list_id = ? AND id=?";
			db.execute(sql,  idList,idItem);
		}
    }

	@Override
	public void delete(TodoList todoList) {
		String sql = "DELETE FROM todo_lists WHERE id=?";
		db.execute(sql,  todoList.getId());
	}




}
