package it.xpug.todolists.repository;

import java.util.*;

import it.xpug.todolists.main.TodoList;

public interface TodoListRepository {

	public TodoList get(int todoListId);
	public int add(TodoList todoList);
	public List<? extends TodoList> getAllOwned(int ownerID);
	public void clear();
	public long size();
	public void update(TodoList todoList);
	public void delete(TodoList todoList);

}
