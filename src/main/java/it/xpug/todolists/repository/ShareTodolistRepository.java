package it.xpug.todolists.repository;

import java.util.List;

import it.xpug.todolists.main.SharedPrivileges;
import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.User;

public interface ShareTodolistRepository {
	public int shareList(TodoList todoL,User target);
	public List<? extends TodoList> getAllSharedWith(int targetID);
	public List<SharedPrivileges<Boolean>> getSharedPrivileges(int todolistID);
	public long size();
	public void clear();
	public void updatePrivileges(TodoList todolistWithPrivileges);
	public void updatePrivileges(int idList, SharedPrivileges<Boolean> sharedPrivileges);
	public void revokeShare(TodoList shared,User target);
	public boolean isSharedTo(TodoList todoList, User target); 
}
