package it.xpug.todolists.repository;

import it.xpug.todolists.main.User;

public interface UserRepository {
	public static enum UserIdentifierType{ID,EMAIL}

	public int add(User user,String clearTextPassword) ;

	public User get(int newUserId);
	
	public User get(String identifier, UserIdentifierType identifierType);

	public User authenticate(String email, String clearTextPassword);

	public void clear();
}
