package it.xpug.todolists.repository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import it.xpug.todolists.main.User;
import it.xpug.toolkit.db.*;

public class DatabaseUserRepository implements UserRepository{

	private Database userDatabase;

	public DatabaseUserRepository(Database database) {
		this.userDatabase = database;
    }
	public void clear() {
		userDatabase.execute("truncate table users cascade");
	}
	@Override
	public int add(User user,String clearTextPassword) {
		String sql = "insert into users (username,email,encryptedPassword,saltForEncryption) values (?,?,?,?) returning id";
		String salt = SecurityToolkit.generateSalt();
		ListOfRows rows = userDatabase.select(sql, user.getField(User.UserFields.NAME),
				user.getField(User.UserFields.EMAIL),
				SecurityToolkit.get_SHA_1_SecurePassword(clearTextPassword, salt),
				salt);
		int id = (int) rows.get(0).get("id");
		return id;
	}

	public User get(int newUserId) {
		ListOfRows rows = userDatabase.select("select * from users where id = ?", newUserId);
		if (rows.size() == 0)
			return null;
		return generateUserFromRow(rows.get(0));
    }

	public static User generateUserFromRow(Map<String, Object> firstRow) {
		int id = (int) firstRow.get("id");
		String name = (String) firstRow.get("username");
		String email=(String) firstRow.get("email");
	    return new User(id,name, email);
	}
	public static List<User> generateUsersFromRows(ListOfRows rows) {
		List<User> result=new LinkedList<User>();
		for (Map<String, Object> map : rows) {
			result.add( generateUserFromRow(map));
		}
		return result;
	}

	public User authenticate(String email, String clearTextPassword) {
		ListOfRows rows = userDatabase.select("select * from users where email = ?", email);
		if (rows.size() == 0)
			return null;
		String salt = (String) rows.get(0).get("saltforencryption");
		if( null==salt){
			return null;
		}
		String encryptedPassword=SecurityToolkit.get_SHA_1_SecurePassword(clearTextPassword, salt);
		rows = userDatabase.select("select * from users where email = ? AND encryptedPassword=?", email,encryptedPassword);
		if (rows.size() == 0)
			return null;
		int id = (int) rows.get(0).get("id");
		String name = (String) rows.get(0).get("username");
		String emailFound=(String) rows.get(0).get("email");
	    return new User(id,name, emailFound);
    }
	@Override
	public User get(String identifier, UserIdentifierType identifierType) {
		switch (identifierType) {
		case ID:
			return get(Integer.parseInt(identifier));
			
		case EMAIL:
			return getByEmail(identifier);
		}
		return null;
	}
	private User getByEmail(String identifier) {
		ListOfRows rows = userDatabase.select("select * from users where email = ?", identifier);
		if (rows.size() == 0)
			return null;
		
		return generateUserFromRow(rows.get(0));
	}

}
