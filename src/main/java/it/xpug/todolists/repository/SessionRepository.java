package it.xpug.todolists.repository;

import it.xpug.todolists.main.TodoListSession;
import it.xpug.todolists.main.User;

public interface SessionRepository {

	public String newSessionId();
	public void add(TodoListSession session);
	public TodoListSession get(String sessionId);
	/**remove all session of the user as argument
	 * @param userFound user witch remove active sessions
	 */
	public void clear(User userFound);

}
