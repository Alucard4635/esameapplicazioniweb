package it.xpug.todolists.resource;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

public class ErrorResurce extends Resource {
	private ResurceException exception;

	public ErrorResurce(HttpServletResponse response, ResurceException exception) {
		super(null, response);
		this.exception = exception;
	}

	@Override
	public void service() throws IOException {
		switch (exception.getType()) {
		case HttpServletResponse.SC_NOT_FOUND:
			response.setStatus(404);
			response.setContentType("application/json");
			response.getWriter().write("{\"error\": \"Not found\"}");
			break;
			
		case HttpServletResponse.SC_UNAUTHORIZED:
			respondWith(403, "Please authenticate");
			break;

		default:
			break;
		}
	}
}
