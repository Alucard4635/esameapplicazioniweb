package it.xpug.todolists.resource;

import static javax.servlet.http.HttpServletResponse.*;

import java.io.*;
import java.util.regex.*;

import javax.servlet.http.*;

import org.json.*;

public abstract class Resource {

	protected HttpServletRequest request;
	protected HttpServletResponse response;
	private Matcher matcher;
	

	public Resource(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
    }
	

	public abstract void service() throws IOException;

	protected void respondWith(int status, String message) throws IOException {
	    response.setStatus(status);
	    render(new JSONObject()
	    	.put("message", message)
	    	.put("status", status));
	}

	protected void notFound(String whatYouSearched) throws IOException {
		respondWith(SC_NOT_FOUND, whatYouSearched+" not found");
	}

	protected String getUriParameter(int position) {
	    return matcher.group(position);
	}

	protected boolean uriMatches(String regex) {
		Pattern pattern = Pattern.compile(regex);
		matcher = pattern.matcher(request.getRequestURI());
		return matcher.matches();
	}
	protected boolean uriMatches(Pattern pattern) {
		matcher = pattern.matcher(request.getRequestURI());
		return matcher.matches();
	}

	protected void render(JSONObject json) throws IOException {
	    response.setContentType("application/json");
	    response.getWriter().write(json.toString(2));
	}

	protected boolean parameterIsMissing(String... parameterName) throws IOException {
		for (int i = 0; i < parameterName.length; i++) {
			if (null == request.getParameter(parameterName[i]) 
					|| request.getParameter(parameterName[i]).isEmpty()) {
				return true;
			}
		}

	    return false;
	}
	
	protected boolean respondIfThisParameterMissing(String... parameterName) throws IOException {
		for (int i = 0; i < parameterName.length; i++) {
			if (null == request.getParameter(parameterName[i]) 
					|| request.getParameter(parameterName[i]).isEmpty()) {
				respondWith(SC_BAD_REQUEST, "a parameter is missing,"+ parameterName[i]+" is required");		
				return true;
			}
		}

	    return false;
	}

	protected boolean isPost() {
	    return "POST".equals(request.getMethod());
	}

	protected int getUriParameterAsInt(int position) {
	    return Integer.parseInt(getUriParameter(position));
	}

}
