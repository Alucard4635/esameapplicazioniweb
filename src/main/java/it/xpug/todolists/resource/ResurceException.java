package it.xpug.todolists.resource;

public class ResurceException extends Exception {

	private static final long serialVersionUID = -2279942100093883306L;
	private Object data;
	private int type;
	//public enum ResourceExceptionType{SESSION_EXPIRED,NOT_FOUND}
	
	public ResurceException(Object data, int type) {
		this.data = data;
		this.type = type;
	}


	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
