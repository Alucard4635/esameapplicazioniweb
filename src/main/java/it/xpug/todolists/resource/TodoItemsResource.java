package it.xpug.todolists.resource;

import it.xpug.todolists.main.TodoListSession;
import it.xpug.todolists.main.SharedPrivileges.PrivilegesType;
import it.xpug.todolists.main.TodoItem;
import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.User;
import it.xpug.todolists.repository.ShareTodolistRepository;
import it.xpug.todolists.repository.TodoListRepository;

import java.io.*;
import java.util.regex.Pattern;

import javax.servlet.http.*;

public class TodoItemsResource extends Resource {

	private TodoListRepository todoLists;
	
	private Pattern allTodoItemPattern=Pattern.compile("/todolists/(\\d+)/items");
	private Pattern todoItemCommandPattern=Pattern.compile("/todolists/(\\d+)/items/(\\d+)/(\\w+)");

	private TodoListSession session;
	private final PrivilegesType CAN_CHECK = PrivilegesType.USE;
	private final PrivilegesType CAN_EDIT = PrivilegesType.UPDATE;


	private ShareTodolistRepository share;




	public TodoItemsResource(HttpServletRequest request, HttpServletResponse response,TodoListSession session, 
			TodoListRepository todoLists, ShareTodolistRepository share) {
	    super(request, response);
		this.todoLists = todoLists;
		this.share = share;
		this.session = session;
    }

	@Override
	public void service() throws IOException {
		if (isPost()) {
			User client = session.getUser();
			if (uriMatches(allTodoItemPattern)) {
				int todoListId = getUriParameterAsInt(1);
				TodoList todoList = getTodolistByID(todoListId);
				if (todoList==null) {
					return;
				}
				createTodoItem(client, todoList);
				return;
			}
	
			if (uriMatches(todoItemCommandPattern)) {
				executeTodoitemCommand(getUriParameter(3),client,getUriParameterAsInt(1),getUriParameterAsInt(2));
				return;
			}
		}
	}

	public void createTodoItem(User client, TodoList todoList)
			throws IOException {
		if (todoList.getOwner().equals(client)||todoList.checkPrivileges(client,CAN_EDIT)) {
			if (!respondIfThisParameterMissing("text")) {
				String textOfNewItem = request.getParameter("text");
				addItem(todoList, textOfNewItem);
			}
		}else {
			respondWith(403, "Unauthorized, you don't have edit privileges on this todolist");
		}
	}
	public void addItem(TodoList todoList, String textOfNewItem) {
		if (textOfNewItem!=null&&!textOfNewItem.isEmpty()) {
			todoList.addItem(new TodoItem(textOfNewItem));
			todoLists.update(todoList);
		}
	}

	public void checkItem(int todolistID,User byHim, TodoList todoList, Boolean checked ) throws IOException {
		if (todoList.getOwner().equals(byHim)||todoList.checkPrivileges(byHim,CAN_CHECK)) {
			todoList.checkItem(todolistID,byHim, checked);
			todoLists.update(todoList);
		}else {
			respondWith(403, "Unauthorized, you don't have check privileges on this todolist");
		}
	}


	public void executeTodoitemCommand(String command, User byHim, int onThisList, int onThisItem) throws IOException {
		TodoList todoList = getTodolistByID(onThisList);
		if (todoList==null) {
			return;
		}
		if (command.equalsIgnoreCase("remove")) {
			removeTodoitem(byHim, todoList, onThisItem);
			return;
		}
		if (command.equalsIgnoreCase("check")) {
			if (!respondIfThisParameterMissing("checked")) {
				Boolean checked = Boolean.valueOf(request.getParameter("checked"));
				checkItem(onThisItem, byHim, todoList, checked);
			}
			return;
		}
		
		respondWith(404, "Todoitem Command Not Found");
		
	}

	private void removeTodoitem(User byHim, TodoList todoList, int onThisItem) throws IOException {
		if (todoList.getOwner().equals(byHim)||todoList.checkPrivileges(byHim,CAN_EDIT)) {
			todoList.removeItem(onThisItem);
			todoLists.update(todoList);
		}else {
			respondWith(403, "Unauthorized, you don't have edit privileges on this todolist");
		}
		return;
	}

	public TodoList getTodolistByID(int todoListId) throws IOException {
			TodoList todoList = todoLists.get(todoListId);
			if (null == todoList) {
				notFound("Todolist");
			}
			todoList.setPrivileges(share.getSharedPrivileges(todoListId));
			return todoList;
	}

}
