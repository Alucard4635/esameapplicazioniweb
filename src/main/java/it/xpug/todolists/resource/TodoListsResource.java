package it.xpug.todolists.resource;

import static java.lang.Integer.*;
import static java.lang.String.*;
import static java.util.Collections.*;
import it.xpug.todolists.main.SharedPrivileges;
import it.xpug.todolists.main.SharedPrivileges.PrivilegesType;
import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.TodoListSession;
import it.xpug.todolists.main.User;
import it.xpug.todolists.main.User.UserFields;
import it.xpug.todolists.repository.ShareTodolistRepository;
import it.xpug.todolists.repository.TodoListRepository;
import it.xpug.todolists.repository.UserRepository;
import it.xpug.todolists.repository.UserRepository.UserIdentifierType;

import java.io.*;
import java.util.regex.Pattern;

import javax.servlet.http.*;

import org.json.*;

public class TodoListsResource extends Resource {

	private UserRepository users;
	private TodoListRepository todoLists;
	private ShareTodolistRepository share;

	private Pattern allTodolistPattern = Pattern.compile("/todolists(/.+)?");
	private Pattern createATodolistPattern = Pattern.compile("/todolists");
	private Pattern todolistPattern = Pattern.compile("/todolists/(\\d+)");
	private Pattern todolistCommandPattern = Pattern.compile("/todolists/(\\d+)/(\\w+)");
	private TodoListSession session;
	
	public TodoListsResource(HttpServletRequest request, HttpServletResponse response,TodoListSession session, TodoListRepository todoLists,
			UserRepository users,  ShareTodolistRepository shareDB) {
		super(request, response);
		this.todoLists = todoLists;
		this.users = users;
		this.session = session;
		this.share = shareDB;
	}

	@Override
	public void service() throws IOException {
		if (uriMatches(allTodolistPattern)) {
			User client = session.getUser();
			if (isPost()) {
				if (uriMatches(todolistCommandPattern)) {
					executeTodolistCommand(getUriParameter(2),client, valueOf(getUriParameter(1)));
					return;
				}
					
				if (uriMatches(createATodolistPattern)) {
					createTodolist( client);
					return;
				}
			}else {
				if (uriMatches(todolistPattern)) {// look into a todolist
					respondWithTodoListAndItems(valueOf(getUriParameter(1)));
					return;
				}
			}
			respondWithAllTodoLists(client);
		}
	}

	public void executeTodolistCommand(String command,User byHim,int onThisTodolist) throws IOException {
		TodoList todoList = getTodolistByID(onThisTodolist);
		if (command.equals("delete")) {
			deleteTodolist(todoList,byHim);
			respondWithAllTodoLists(byHim);
			return;
		}
		User target = getTargetByParameterEmail();
		if (target!=null) {
			if (command.equalsIgnoreCase("share")) {//getUriParameter(2)
				System.out.println(">Sharing "+todoList.getId()+" to "+target.getField(User.UserFields.EMAIL) );
				shareTodolist(todoList,byHim,target);//valueOf(getUriParameter(1))
				return;
			}
			if (command.equals("revoke")) {
				revokeTodolist(todoList,byHim,target);
				return;
			}
			if (command.equals("setPrivileges")) {
				setSharePrivilege(valueOf(getUriParameter(1)), byHim, target);
				return;
			}
			respondWith(404, "Todolist Command Not Found");
		}else {
			return;
		}
	}

	private void deleteTodolist(TodoList todoList, User byHim) throws IOException {
		if (todoList.getOwner().equals(byHim)) {
			todoLists.delete(todoList);
		}else {
			respondWith(HttpServletResponse.SC_UNAUTHORIZED, "Only owners can delete lists");
		}
		
	}

	public User getTargetByParameterEmail() throws IOException {
		if (respondIfThisParameterMissing("emailTarget")) {
			return null;
		}
		String emailTarget=request.getParameter("emailTarget");
		User target = users.get(emailTarget, UserIdentifierType.EMAIL);
		if (target==null) {
			notFound("User eMail");
		}
		return target;
	}

	private void revokeTodolist(TodoList todoList, User byHim, User target) throws IOException {
		if (todoList.isAdmin(byHim)||byHim.equals(target)) {
			share.revokeShare(todoList, target);
			renderTodolist(todoList); 

		}else {
			respondWith(403, "Unauthorized, you don't have Admin privileges on this todolist");
		}
	}

	private void setSharePrivilege(Integer todolistID, User client, User target) throws IOException {
		TodoList todolist = getTodolistByID(todolistID);
		todolist.setPrivileges(share.getSharedPrivileges(todolist.getId()));

		if (todolist.isAdmin(client)) {
			if (!respondIfThisParameterMissing("privilege","statusPrivilege")) {
				PrivilegesType[] enums = PrivilegesType.values();
				String privilegeType = request.getParameter("privilege");
				boolean howToSet = request.getParameter("statusPrivilege").equals("true");
				int privilegeIndex = Integer.parseInt(privilegeType);
				
				SharedPrivileges<Boolean> updatedPrivilege = todolist.setPrivilege(
						target,
						howToSet,
						enums[privilegeIndex]);
				
				share.updatePrivileges(todolistID, updatedPrivilege);
				renderTodolist(todolist); 
			}
		}else {
			respondWith(403, "Unauthorized, you don't have admin privileges on this todolist");
		}
	}

	private void createTodolist(User client)
			throws IOException {
		if (respondIfThisParameterMissing("name")) {
			return;
		}
		/*int newTodoListId =*/ todoLists.add(new TodoList(client, request.getParameter("name")));
		//response.sendRedirect("/todolists/" + newTodoListId);
	}

	private void shareTodolist(TodoList todoList,User whoShare, User target) throws IOException {
		User owner = todoList.getOwner();
		if (owner.equals(whoShare)||todoList.checkPrivileges(whoShare, PrivilegesType.SHARE)) {
			if (owner.equals(target)) {
				respondWith(HttpServletResponse.SC_CONFLICT, "This email is of owner's one");
			}else {
				if (share.isSharedTo(todoList,target)) {
					respondWith(HttpServletResponse.SC_CONFLICT, "This list is already shared to "+target.getField(UserFields.EMAIL));
				}else{
					share.shareList(todoList, target);
					renderTodolist(todoList); 
				}
			}
		}else {
			respondWith(403, "Unauthorized, you don't have share privileges on this todolist");
		}
	}



	private void respondWithAllTodoLists(User client) throws IOException {
		JSONObject json = new JSONObject();
		json.put("myLists", emptyList());
		for (TodoList todoList : todoLists.getAllOwned(client.getId())) {
			json.append("myLists", new JSONObject()
					.put("todolist", todoList.toJSON())
					.put("uri",format("/todolists/%d",  todoList.getId()))
			);
		}
		json.put("sharedWithMe", emptyList());
		for (TodoList todoList : share.getAllSharedWith(client.getId())) {
			json.append("sharedWithMe", new JSONObject()
					.put("todolist", todoList.toJSON())
					.put("uri",format("/todolists/%d",  todoList.getId()))
			);
		}
		render(json);
	}

	private void respondWithTodoListAndItems(Integer todoListId) throws IOException {
		TodoList todoList = getTodolistByID(todoListId);
		renderTodolist(todoList);
	}

	private TodoList getTodolistByID(Integer todoListId) throws IOException {
		TodoList todoList = todoLists.get(todoListId);
		if (null == todoList) {
			notFound("Todolist");
		}
		todoList.setPrivileges(share.getSharedPrivileges(todoListId));
		return todoList;
	}

	private void renderTodolist( TodoList todoList)
			throws IOException {
		todoList.setPrivileges(share.getSharedPrivileges(todoList.getId()));
		JSONObject todolistJson = todoList.toJSON();
		JSONArray items = (JSONArray) todolistJson.get("items");
		for (int itemId = 0; itemId < items.length(); itemId++) {
			JSONObject todoItem = (JSONObject) items.get(itemId);
			todoItem.put("uri", format("/todolists/%d/items/%d", todoList.getId(), itemId));
		}
		render(todolistJson);
	}

}
