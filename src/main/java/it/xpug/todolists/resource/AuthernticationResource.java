package it.xpug.todolists.resource;

import it.xpug.todolists.main.AuthenticationFilter;
import it.xpug.todolists.main.TodoListSession;
import it.xpug.todolists.main.User;
import it.xpug.todolists.repository.SessionRepository;
import it.xpug.todolists.repository.UserRepository;

import java.io.*;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.servlet.http.*;

import org.json.JSONObject;

public class AuthernticationResource extends Resource {
	private UserRepository users;
	private SessionRepository sessions;
	/*
	 * private Pattern
	 * sessionPattern=Pattern.compile("/todolists/login");//[/]?(\\w+)?");
	 * private Pattern registerPattern=Pattern.compile("/todolists/register");
	 */
	private TodoListSession session = null;

	public AuthernticationResource(HttpServletRequest request, HttpServletResponse response, SessionRepository sessions,
			UserRepository users) {
		super(request, response);
		this.sessions = sessions;
		this.users = users;
	}

	public AuthernticationResource(HttpServletRequest request, HttpServletResponse response, TodoListSession session,
			SessionRepository sessions, UserRepository users) {
		super(request, response);
		this.session = session;
		this.sessions = sessions;
		this.users = users;
	}

	@Override
	public void service() throws IOException {
		if (isPost()) {
			if (session!=null) {
				sessions.clear(session.getUser());
				response.addCookie(generateSessionCookie(session.getId(), 0));
			} else {

				String username = request.getParameter("username");

				String email = request.getParameter("email");
				String clearTextPassword = request.getParameter("password");
				if (username == null) {
					System.out.println("Login: " + email);
					authenticate(email, clearTextPassword);
				} else {
					if (username.length() <= 0) {
						render(new JSONObject().put("message", "Please insert Username"));
						return;
					} else {
						System.out.println("Register: " + username + " " + email);
						register(username, email, clearTextPassword);
					}
				}
			}
		} else {
			
			// String sessionID = getUriParameter(1);
			// TodoListSession todoListSession = sessions.get(sessionID);
			TodoListSession todoListSession = getSession(request.getCookies());
			if (todoListSession != null) {
				User userFound = todoListSession.getUser();
				login(userFound);

			} else {
				response.setStatus(403);
				render(new JSONObject().put("message", "Session expired, please log in"));
				return;
			}

		}
	}

	public TodoListSession getSession(Cookie[] cookies) {
		if (null == cookies)
			return null;
		for (int i = 0; i < cookies.length; i++) {
			Cookie cookie = cookies[i];
			if (cookie.getName().equals(AuthenticationFilter.SESSION_COOKIE)) {
				return sessions.get(cookie.getValue());
			}
		}
		return null;
	}

	private void register(String username, String email, String clearTextPassword) throws IOException {
		User user = new User(username, email);
		try {
			user.setId(users.add(user, clearTextPassword));
			// login(email, clearTextPassword);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(403);
			render(new JSONObject().put("message", "Email already taken"));
		}
	}

	private void authenticate(String email, String password) throws IOException {
		User userFound = users.authenticate(email, password);
		if (userFound != null) {
			login(userFound);
		} else {
			response.setStatus(403);
			render(new JSONObject().put("message", "Email or password not valid"));
		}
	}

	public void login(User userFound) throws IOException {
		createANewSession(userFound);
		renderUser(userFound);
	}

	public void createANewSession(User userFound) {
		sessions.clear(userFound);
		String newSessionId = sessions.newSessionId();
		GregorianCalendar now = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		sessions.add(new TodoListSession(newSessionId, userFound, request.getRemoteAddr(), now, now));
		response.addCookie(generateSessionCookie(newSessionId, 7));
	}

	private void renderUser(User userFound) throws IOException {
		JSONObject newJsonObject = new JSONObject();
		render(newJsonObject.put("user", userFound.toJSON()));
	}

	private Cookie generateSessionCookie(String newSessionId, int daysOfLife) {
		Cookie cookie = new Cookie(AuthenticationFilter.SESSION_COOKIE, newSessionId);
		cookie.setMaxAge(3600 * 24 * daysOfLife);
		return cookie;
	}

}
