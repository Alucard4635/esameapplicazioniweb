package it.xpug.todolists.main;

import java.util.GregorianCalendar;

import it.xpug.todolists.main.User.UserFields;

public class TodoListSession {

	private String id;
	private User user;
	private String ipAddress;
	private GregorianCalendar creationDate;
	private GregorianCalendar updateDate;

	public TodoListSession(String id, User user, String ip, GregorianCalendar creationDate, GregorianCalendar updateDate) {
		this.id = id;
		this.user = user;
		this.ipAddress = ip;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
	}

	public TodoListSession(String id, User user, String ip) {
		this(id,user,ip,new GregorianCalendar(),new GregorianCalendar());
	}


	public String getId() {
	    return id;
    }

	public User getUser() {
		return user;
	}

	public String getIpAddress() {
		return ipAddress;
	}



	public GregorianCalendar getCreationDate() {
		return creationDate;
	}

	public GregorianCalendar getUpdateDate() {
		return updateDate;
	}

	@Override
	public String toString() {
		return id.toString()+user.getField(UserFields.EMAIL)+ipAddress+creationDate;
	}
}
