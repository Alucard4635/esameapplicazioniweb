package it.xpug.todolists.main;

import it.xpug.todolists.repository.SessionRepository;
import it.xpug.todolists.repository.ShareTodolistRepository;
import it.xpug.todolists.repository.TodoListRepository;
import it.xpug.todolists.repository.UserRepository;
import it.xpug.todolists.resource.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class TodoListsServlet extends HttpServlet {

	private static final long serialVersionUID = 3694701858714257395L;
	private TodoListRepository todoLists;
	private AuthenticationFilter authenticationFilter;
	private SessionRepository sessions;
	private UserRepository users;
	private ShareTodolistRepository share;

	public TodoListsServlet(TodoListRepository todoLists, SessionRepository sessions, UserRepository users,
			ShareTodolistRepository share) {
		this.todoLists = todoLists;
		this.sessions = sessions;
		this.users = users;
		this.share = share;
		this.authenticationFilter = new AuthenticationFilter(sessions);
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Serving: " + request.getMethod() + " " + request.getRequestURI());
//		delay();

		Resource resource = getResource(request, response);
		resource.service();
	}

	protected void delay() {
		try {
			Thread.sleep(700);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	private Resource getResource(HttpServletRequest request, HttpServletResponse response) {
		String requestURI = request.getRequestURI();
		if (requestURI.matches("/todolists/login")) {
			return new AuthernticationResource(request, response, sessions, users);
		}
		if (requestURI.matches("/todolists/register")) {
			return new AuthernticationResource(request, response, sessions, users);
		}

		TodoListSession session = authenticationFilter.getSession(request.getCookies());
		if (null == session) {
			return invalidSession(response);
		}

		if (requestURI.matches("/todolists/logout")) {
			return new AuthernticationResource(request, response, session, sessions, users);
		}

		if (requestURI.matches("/todolists/\\d+/items(/\\d+(/.+)?)?")) {
			return new TodoItemsResource(request, response, session, todoLists, share);
		}
		if (requestURI.matches("/todolists(/.+)?")) {
			return new TodoListsResource(request, response, session, todoLists, users, share);
		}
		if ("/".equals(requestURI)) {
			return new HelloWorldResource(response);
		}
		return notFound(response);
	}

	public ErrorResurce invalidSession(HttpServletResponse response) {
		return new ErrorResurce(response, new ResurceException(null, HttpServletResponse.SC_UNAUTHORIZED));
	}

	public ErrorResurce notFound(HttpServletResponse response) {
		return new ErrorResurce(response, new ResurceException(null, HttpServletResponse.SC_NOT_FOUND));
	}

}
