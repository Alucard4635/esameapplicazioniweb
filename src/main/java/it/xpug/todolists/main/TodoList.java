package it.xpug.todolists.main;

import static java.util.Collections.*;

import java.sql.Timestamp;
import java.util.*;

import org.json.*;

import it.xpug.todolists.main.SharedPrivileges.PrivilegesType;

public class TodoList {
	private int id;
	private User owner;
	private String name;
	private List<TodoItem> todoItems = new ArrayList<TodoItem>();
	private List<TodoItem>	removedItems= new LinkedList<TodoItem>();
	private List<SharedPrivileges<Boolean>> privileges= new ArrayList<SharedPrivileges<Boolean>>();


	public TodoList(int todoListId, User owner, String name) {
		id = todoListId;
		this.owner = owner;
		this.name = name;
    }

	public TodoList(User owner, String name) {
		id = -1;
		this.owner = owner;
		this.name = name;	
	}

	public String getName() {
	    return name;
    }

	public void addItem(TodoItem todoItem) {
		todoItems.add(todoItem);
    }
	
	public void removeItem(int todoItemID){
		TodoItem remove = todoItems.remove(todoItemID);
		if (remove!=null) {
			removedItems.add(remove);
		}
	}

	public JSONObject toJSON() {
	    JSONObject result = new JSONObject()
    		.put("owner", getOwner().toJSON())
	    	.put("name", getName())
	    	.put("items", emptyList())
	    	.put("shareWith",emptyList());
	    for (TodoItem todoItem : todoItems) {
	        result.append("items", todoItem.toJSON());
        }
	    for (SharedPrivileges<Boolean> sharedPrivileges : privileges) {
	        result.append("shareWith", sharedPrivileges.toJSON());
		}
	    return result;
    }

	public void checkItem(int todoItemId, User byHim, boolean checked) {
		TodoItem todoItem = todoItems.get(todoItemId);
		todoItem.setChecked(checked);
		todoItem.setWhoCheck(byHim);
		todoItem.setTimeOfCheck(new Timestamp(System.currentTimeMillis()));
    }

	public int getId() {
	    return id;
    }

	public void setId(int id) {
		this.id = id;
    }

	public List<TodoItem> getItems() {
	    return new ArrayList<TodoItem>(todoItems);
    }

	public int getOwnerID() {
		return owner.getId();
	}

	public User getOwner() {
		return owner;
	}

	public List<SharedPrivileges<Boolean>> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<SharedPrivileges<Boolean>> whoCanWhat) {
		this.privileges = whoCanWhat;
	}
	public SharedPrivileges<Boolean> setPrivilege(User target,boolean enable,PrivilegesType type ){
		SharedPrivileges<Boolean> privilege = this.getPrivilege(target);
		if (privilege!=null) {
			privilege.setPrivilege(type, enable);
		}
		return privilege;
	}

	public boolean isAdmin(User client) {
		if (owner.equals(client)|| checkPrivileges(client,SharedPrivileges.PrivilegesType.ADMIN)) {
			return true;
		}
		return false;
	}

	public boolean checkPrivileges(User client, PrivilegesType type) {
		SharedPrivileges<Boolean> privilege = getPrivilege(client);
		if (privilege==null) {
			return false;
		}
		return privilege.getPrivilege(type);
	}

	private SharedPrivileges<Boolean> getPrivilege(User client) {
		User target = null;
		for (SharedPrivileges<Boolean> sharedPrivileges : privileges) {
			target = sharedPrivileges.getTarget();
			if (target.equals(client)) {
				return sharedPrivileges;
			}
		}
		return null;
	}

	public List<TodoItem> getRemovedItems() {
		List<TodoItem> result = new LinkedList<TodoItem>( removedItems);
		return result;
	}


	public void setItems(List<TodoItem> todoItems) {
		this.todoItems = todoItems;
	}

	
	

}
