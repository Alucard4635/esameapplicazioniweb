package it.xpug.todolists.main;

import org.json.JSONObject;

public class User {
	public static enum UserFields{NAME,EMAIL, ID}
	private int id;
	private String name;
	private String email;

	public User(String name, String email) {
		this.id = -1;
		this.name = name;
		this.email = email;
	}
	
	public User(int id,String name, String email) {
		this.id = id;
		this.name = name;
		this.email = email;
    }

	public String getField(UserFields field) {
		switch (field) {
		case ID:{
			return Integer.toString(id);
		}
		case NAME:{
			return name;
			}
		case EMAIL:{
			return email;
		}
		default:
			throw new IllegalArgumentException();
    }
	}
	public JSONObject toJSON() {
		JSONObject userToJsonObject = new JSONObject();
		UserFields[] enums = UserFields.values();
		for (UserFields userFields : enums) {
			userToJsonObject.put(userFields.toString().toLowerCase(), getField(userFields));
		}
		return userToJsonObject;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.toJSON().toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			User other=(User) obj;
			UserFields[] enums = UserFields.values();
			for (UserFields field : enums) {
				if (!this.getField(field).equals(other.getField(field))) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

}
