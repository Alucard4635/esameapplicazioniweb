package it.xpug.todolists.main;

import static java.lang.Integer.*;
import it.xpug.todolists.repository.DatabaseSessionRepository;
import it.xpug.todolists.repository.DatabaseShareTodolistRepository;
import it.xpug.todolists.repository.DatabaseTodoListRepository;
import it.xpug.todolists.repository.DatabaseUserRepository;
import it.xpug.todolists.repository.SessionRepository;
import it.xpug.todolists.repository.ShareTodolistRepository;
import it.xpug.todolists.repository.TodoListRepository;
import it.xpug.todolists.repository.UserRepository;
import it.xpug.toolkit.db.*;
import it.xpug.toolkit.web.*;

public class Main {
	private static final String DATABASE_URL = "postgres://egkmtelsgzlqvk:2y4irH__BIAyBHWGUORMBlZImV@ec2-54-83-40-119.compute-1.amazonaws.com:5432/dalrjqgcmqf87i";
//	private static final String DATABASE_URL = "postgres://todolists:secret@localhost:5432/todolists_development";


	public static void main(String[] args) {
		String port = System.getenv("PORT");
		if (port == null || port.isEmpty()) {
			port = "8080";
		}

		DatabaseConfiguration configuration = new DatabaseConfiguration(DATABASE_URL);
		Database db = new Database(configuration);
		UserRepository users = new DatabaseUserRepository(db);
		TodoListRepository todoLists = new DatabaseTodoListRepository(db, users);
		ShareTodolistRepository share = new DatabaseShareTodolistRepository(db, users);
		SessionRepository sessionRepository = new DatabaseSessionRepository(db, users);

		ReusableJettyApp app = new ReusableJettyApp(new TodoListsServlet(todoLists, sessionRepository, users, share));

		app.start(valueOf(port), "src/main/webapp");
	}
}
