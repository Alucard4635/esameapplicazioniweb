package it.xpug.todolists.main;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import org.json.*;

public class TodoItem {
	private static DateFormat df = getFrormatter();
	public static SimpleDateFormat getFrormatter() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return simpleDateFormat;
	}
	private String text;
	private boolean isChecked;
	private Integer id;
	private User whoCheck;
	private Timestamp timeOfCheck;


	public TodoItem(String text) {
	    this.text = text;
    }

	public TodoItem(int todoItemId, String text) {
		id = todoItemId;
		this.text = text;
    }

	public TodoItem(int todoItemId, String text, boolean isChecked) {
		id = todoItemId;
		this.text = text;
		this.isChecked = isChecked;
    }

	public JSONObject toJSON() {
		JSONObject json = new JSONObject()
	    	.put("text", text)
	    	.put("status", isChecked ? "checked" : "unchecked");
		if (timeOfCheck!=null&&whoCheck!=null) {
			String format = df.format(timeOfCheck);
			json.put("timeOfLastCheck", format)
				.put("whoChecked", whoCheck.toJSON());
		}
		return json;


    }

	public boolean isChecked() {
	    return isChecked;
    }

	public void setChecked(boolean checked) {
		isChecked = checked;
    }

	public Integer getId() {
	    return id;
    }

	public String getText() {
	    return text;
    }
	@Override
	public String toString() {
		return toJSON().toString();
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TodoItem) {
			TodoItem other=(TodoItem) obj;
			return this.id==other.id;
		}
		return super.equals(obj);
	}

	public User getWhoCheck() {
		return whoCheck;
	}

	public void setWhoCheck(User whoCheck) {
		this.whoCheck = whoCheck;
	}

	public Timestamp getTimeOfCheck() {
		return timeOfCheck;
	}

	public void setTimeOfCheck(Timestamp timeOfCheck) {
		this.timeOfCheck = timeOfCheck;
	}
}
