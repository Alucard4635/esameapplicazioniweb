package it.xpug.todolists.main;

import org.json.JSONArray;
import org.json.JSONObject;


public class SharedPrivileges<T> {
	public static enum PrivilegesType{ADMIN,USE,UPDATE,SHARE};
	private User target;
	private Object[] privileges;
	
	public SharedPrivileges(User target) {
		this.target = target;
		privileges=new Object[PrivilegesType.values().length];
	}
	@SafeVarargs
	public SharedPrivileges( User target,T...bs){
		this.target = target;
		privileges=new Object[PrivilegesType.values().length];
		for (int i = 0; i < bs.length||i<privileges.length; i++) {
			privileges[i]=bs[i];
		}
	}
	
	public void setPrivilege(PrivilegesType type,T canDo){
		privileges[type.ordinal()]=canDo;
	}
	@SuppressWarnings("unchecked")
	public T getPrivilege(PrivilegesType type){
		return (T) privileges[type.ordinal()];
	}
	public JSONObject toJSON() {
	    JSONObject result = new JSONObject();
	    result.put("target", target.toJSON());
	    JSONArray privilegesJson=new JSONArray();
	    for (int i = 0; i < privileges.length; i++) {
	    	privilegesJson.put(i, privileges[i]);
		}
	    result.put("privileges", privilegesJson);
		return result;
	}
	public User getTarget() {
		return target;
	}
	public void setTarget(User target) {
		this.target = target;
	}
}
