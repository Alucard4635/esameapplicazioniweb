$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
	loginWithSessionRequest();
	setEvents();
});
function setEvents() {
	$('#shareLink').click(toggleShareWith);
	$('#new-list-link').click(toggleNewListForm);
	$('#new-list-cancel').click(toggleNewListForm);
	$('#editLists').click(toggleEditListMode);
	$('#error').click(hide_error);
	$('#editLink').click(toggleEditItemMode);
	$('#logOut').click(logOut);
	$('#login-form').submit(authenticate);
	$('#register-form').submit(registerRequest);
	$('#shareWithForm').submit(shareTodolist);
	$('#new-list-form').submit(create_new_list);
	$('#new-todo-item-form').submit(create_new_todo_item);
	$('.toggle').click(toggleLoginRegister);
	$('form').submit(clearAllFields);
}
function clearAllFields() {
	$("input[type='text']").val('');
	$("input[type='password']").val('');
}

var loginMode = true;
function toggleLoginRegister() {
	// Switches the Icon
	$(this).children().toggleClass('fa-pencil');
	loginAnimation()
	if (loginMode) {
		$('#haveAnAccount').html('Already registered?');
		loginMode = false;
	} else {
		$('#haveAnAccount').html("Don't you have an account?");
		loginMode = true;
	}

}



function shareTodolist() {
	var email = $('#shareWithInput').val();
	if (validateEmail(email)) {
		$('#shareIncorrectInput').hide();
		shareRequest(email);
	} else {
		$('#shareIncorrectInput').show(200);
		$('#shareIncorrectInput').delay(3000).hide(500);
	}
	return false;
}
function shareRequest(email) {
	startShareListLoading();
	$.ajax({
		url : current_todo_list_uri + "/share",
		method : 'POST',
		success : loadSharedPrivilegesList,
		error : on_error,
		data : {
			emailTarget : email,
		},
	});

	return false;
}

function loadSharedPrivilegesList(data) {
	var template = $('#sharedWithTemplate').html();
	$('#sharedWithList').html('');
	$('#new-todo-item-form').show();
	var canModifiedPrivileges = false;

	for (var i = 0; i < data.shareWith.length; i++) {
		var current = data.shareWith[i];
		var disable = "";
		var hidden = "hidden";

		var isAdmin = current.privileges[0];
		var isAdminChecked = isAdmin ? "checked" : "";

		var canUse = current.privileges[1];
		var canCheckChecked = canUse ? "checked" : "";

		var canUpdate = current.privileges[2];
		var canAddItemsChecked = canUpdate ? "checked" : "";

		var canShare = current.privileges[3];
		var canShareTodolist = canShare ? "checked" : "";

		if (current.target.email == userInfo.email) {
			hidden = "";
			canModifiedPrivileges = isAdmin;
			if (!canUpdate) {
				$('#new-todo-item-form').hide();
			}
			$('.todo-item').prop('disabled', !canUse);
			if (!canShare) {
				$('#shareWithForm').hide();
				if (isAdmin) {
					disable = "disabled";// he is admin, can create a new user,
										// share the list to him then add to him
										// privileges
				}
			} else {
				$('#shareWithForm').show();

			}
		}

		var html = Mustache.render(template, {
			sharedWithList : data.shareWith[i],
			canCheck : canCheckChecked,
			canAddItems : canAddItemsChecked,
			adminOption : isAdminChecked,
			canShare : canShareTodolist,
			disabled : disable,
			hideRevoke : hidden,
			me : userInfo,
		});
		$('#sharedWithList').append(html);
		stopShareListLoading();
	}
	if (canModifiedPrivileges || data.owner.email == userInfo.email) {
		$("input.PrivilegeCheckbox").click(togglePrivilegesRequest);
		$("a.revokeLink").prop('hidden', false);
	} else {
		$("input.PrivilegeCheckbox").prop('disabled', true);
	}
	$("a.revokeLink").click(revokeRequest);
	stopShareListLoading();
}

function revokeRequest() {
	startShareListLoading();
	$.ajax({
		url : current_todo_list_uri + "/revoke",
		method : 'POST',
		success : loadSharedPrivilegesList,
		error : on_error,
		data : {
			emailTarget : $(this).siblings('.people').attr('data-targetEmail'),
		},
	});
}

function togglePrivilegesRequest() {
	var value = false;
	if ($(this).attr('checked') == undefined) {
		value = true;
	}
	$.ajax({
		url : current_todo_list_uri + "/setPrivileges",
		method : 'POST',
		success: loadSharedPrivilegesList,
		error : on_error,
		data : {
			emailTarget : $(this).siblings('.people').attr('data-targetEmail'),
			privilege : $(this).attr('data-privilegeType'),
			statusPrivilege : value,
		},
	});
}

function loginWithSessionRequest(sessionID) {
	$.ajax({
		url : '/todolists/login',
		success : setLogged,
		error : logOut,
	});
	return false;
}

function logOut(data) {
	logOutRequest();
	deleteCookie('todolists_session_id');
	setNotLogged();
}
function logOutRequest() {
	$.ajax({
		url : '/todolists/logout',
		method : 'POST',
		success : setNotLogged,
		data : {},
	});
	return false;
}
function setNotLogged() {
	$('#userLogged').hide();
	setOneListMode();
	$('#userNotLogged').show();

}
function deleteCookie(name) {
	setCookie(name, "", 0);
}

function registerRequest() {
	var usernameInsert = $('#registerUsername').val();
	var emailInsert = $('#registerEmail').val();
	if (validateEmail(emailInsert)) {

		var passwordInsert = $('#registerPassword').val();
		var passwordRepeated = $('#registerPasswordRepeated').val();
		if(passwordInsert.length>=8){
			if (passwordInsert == passwordRepeated) {
				$.ajax({
					url : '/todolists/register',
					method : 'POST',
					success: registrationSuccess,
					error : on_error,
					data : {
						username : usernameInsert,
						email : emailInsert,
						password : passwordInsert,
					},
				});
			} else {
				showErrorDialog("passwords mismatch")
			}
		}else {
			showErrorDialog("passwords too short, min 8 character")
		}
	} else {
		showErrorDialog("invalid eMail address")
	}
	return false;
}
function showErrorDialog(message){
	$('#error').text(message).show();
	stopTodoListLoading()
}

function authenticate(e){
	e.preventDefault();
	loginRequest($('#login-email').val(), $('#login-password').val());
}

function registrationSuccess(){
	$('#registrationSuccess').show(100).delay(2000).fadeOut(500);
}

function loginRequest(emailToSend,passwordToSend) {
	console.log('login '+emailToSend+" "+passwordToSend);
	$.ajax({
		url : '/todolists/login',
		method : 'POST',
		success : setLogged,
		error : on_error,
		data : {
			email : emailToSend,
			password : passwordToSend,
		},
	});
	return false;
}
var userInfo;
function setLogged(data) {
	$('#userNotLogged').animate({
		height : "hide",
		'padding-top' : 'hide',
		'padding-bottom' : 'hide',
		opacity : "hide"
	}, "slow");
	setWelcomeMessage(data);
	userInfo = data.user;
	populateListsRequest();
	$('#userLogged').show();

}
function setWelcomeMessage(data) {
	var template = $('#welcomeMessageTemplate').html();
	var html = Mustache.render(template, {
		welcomeMessage : data,
	});
	$('#welcomeMessage').html(html);
}

function on_error(data) {
	console.log(JSON.stringify(data, null, 2));
	var message = data.statusText;
	if (typeof data.responseJSON !== 'undefined'){
		message += ': ' + data.responseJSON.message;
		}
	$('#error').text(message).show();
	stopTodoListLoading()
	return false;
}

function hide_error() {
	$('#error').hide(200);
}

function create_new_list() {
	$.ajax({
		url : '/todolists',
		method : 'POST',
		success : on_create_new_list_success,
		error : on_error,
		data : {
			name : $('#new-list-name').val()
		},
	});
	return false;
}

function on_create_new_list_success() {
	$('#new-list-name').val("");
	toggleNewListForm();
	populateListsRequest();
}

function populateListsRequest() {
	startMyListsLoading();
	startSharedWithMeLoading()
	$.ajax({
		url : '/todolists',
		success : populateListSuccess,
		error : on_error,
	});
	return false;

}

function populateListSuccess(data) {
	populateMyList(data);
	stopMyListsLoading();

	populateSharedWithMe(data);
	stopSharedWithMeLoading()
	$('a.todolist').click(open_todo_list);
}
var todolistInfoSeparator=' -';
function populateMyList(data) {
	var template = $('#my-lists-template').html();
	$('#my-lists').html('');
	var current;
	var html;
	var itemInfo;
	for (var i = 0; i < data.myLists.length; i++) {
		current = data.myLists[i];
		itemInfo = getItemInfo(current.todolist.items);
		html = Mustache.render(template, {
			uri : current,
			my_lists : current,
			info : todolistInfoSeparator+itemInfo,
		});
		$('#my-lists').append(html);
	}
	$('.deleteList').click(deleteListRequest);
}
function deleteListRequest() {
	$.ajax({		
		method : 'POST',
		url : $(this).attr('data-uri')+"/delete",
		success : populateMyList,
		error : on_error,
	});
}

function populateSharedWithMe(data) {
	var template = $('#sharedWithMeListsTemplate').html();
	$('#sharedWithMeLists').html('');
	var current;
	var html;
	var itemInfo;
	for (var i = 0; i < data.sharedWithMe.length; i++) {
		current = data.sharedWithMe[i];
		itemInfo = getItemInfo(current.todolist.items);
		html = Mustache.render(template, {
			uri : current,
			sharedWithMeLists : current,
			info : todolistInfoSeparator+itemInfo,
			shareInfo : "(from " + current.todolist.owner.name+")",
		});
		$('#sharedWithMeLists').append(html);
	}

}

function getItemInfo(itemsJsonArray) {
	var currentItem;
	var itemsLeft = 0;
	var isEmpty = true
	for (var itemIndex = 0; itemIndex < itemsJsonArray.length; itemIndex++) {
		currentItem = itemsJsonArray[itemIndex];
		if (currentItem.status == "unchecked") {
			itemsLeft++;
		}
		isEmpty = false;
	}
	if (isEmpty) {
		itemInfo = "Empty";
	} else {
		if (itemsLeft == 0) {
			itemInfo = "Completed";
		} else {
			itemInfo = itemsLeft + " left";
		}
	}
	return itemInfo;
}
var current_todo_list_uri;

function open_todo_list() {
	current_todo_list_uri = $(this).attr('data-uri');
	openTodoListRequest();
}

function openTodoListRequest() {
	startTodoListLoading();
	$.ajax({
		url : current_todo_list_uri,
		success : updateTodoItems,
		error : on_error,
	});

	return false;
}

function updateTodoItems(todolistJSON) {
	setOneListMode(todolistJSON.name);
	var template = $('#todoItemsTemplate').html();
	var current, is_checked, checked_attr, html;
	var timeOfCheck, dayToSend, monthToSend, fullYearToSend, hourToSend, minuteToSend;
	var hideInfo, hideMe, hideChecker;

	for (var i = 0; i < todolistJSON.items.length; i++) {
		hideInfo = "hidden";
		hideChecker = "";
		hideMe = "hidden"

		dayToSend = undefined;
		monthToSend = undefined;
		fullYearToSend = undefined;
		hourToSend = undefined;
		minuteToSend = undefined;

		current = todolistJSON.items[i];

		is_checked = current.status == "checked";
		checked_attr = is_checked ? "checked" : "";

		if (current.timeOfLastCheck != undefined) {
			hideInfo = "";
			timeOfCheck = getThisTime(new Date(current.timeOfLastCheck));
			if (!isDaylightSaving(timeOfCheck)) {
				timeOfCheck.setMilliseconds(timeOfCheck.getMilliseconds()-3600000)
			}
			dayToSend = timeOfCheck.getDate();
			monthToSend = timeOfCheck.getMonth() + 1;
			fullYearToSend = timeOfCheck.getFullYear();
			hourToSend = timeOfCheck.getHours();
			minuteToSend = timeOfCheck.getMinutes();
		}
		if (current.timeOfLastCheck != undefined
				&& current.whoChecked.email == userInfo.email) {
			hideChecker = "hidden";
			hideMe = "";
		}
		html = Mustache.render(template, {
			todoItem : current,
			time : {
				day : dayToSend,
				month : monthToSend,
				fullYear : fullYearToSend,
				hour : hourToSend,
				minute : minuteToSend,
			},
			hiddenInfo : hideInfo,
			hiddenChecker : hideChecker,
			hiddenMe : hideMe,
			me: userInfo,
		});
		if (is_checked) {
			$('#todo-items-checked').append(html);
		} else {
			$('#todo-items-unchecked').append(html);
		}
		;
	}
	$('.removeItem').click(removeItem);
	$('.todo-item').click(toggle_check);
	$('.todoItemName').click(toggle_check);

	loadSharedPrivilegesList(todolistJSON);
	populateListsRequest();
	stopTodoListLoading();

}
function getThisTime(utc) {
	return calcTime(utc, new Date().getTimezoneOffset()*-1)
}
function calcTime(utc, offset) {
	utcMills = utc.getTime();
	date = new Date(utcMills + (60000 * offset));
	return date;
}
function isDaylightSaving(date){
	var currentYear = date.getFullYear();

	// DST Start
	var firstOfMarch = new Date(currentYear, 2, 1);
	var daysUntilFirstSundayInMarch = (7 - firstOfMarch.getDay()) % 7;
	var secondSundayInMarch = firstOfMarch.getDate() + daysUntilFirstSundayInMarch + 7;
	var dstStartDate = new Date(currentYear, 2, secondSundayInMarch);

	// DST End
	var firstOfNovember = new Date(currentYear, 10, 1);
	var daysUntilFirstSundayInNov = (7 - firstOfNovember.getDay()) % 7;
	var firstSundayInNovember = firstOfNovember.getDate() + daysUntilFirstSundayInNov;
	var dstEndDate = new Date(currentYear, 10, firstSundayInNovember);
	
	if (date.getTime()-dstStartDate.getTime()>0) {
		if (date.getTime()-dstEndDate.getTime()<0) {
			return true;
		}
	}
	return false;
}

function setOneListMode(todolistName) {
	if (todolistName == undefined || todolistName == "") {
		$('#my-lists-section').show();
		$('#sharedWithMeListsSection').show();
		$('#one-list-section').hide();
		$('#one-list-section h2').text("");
	} else {
		// $('#my-lists-section').hide();
		// $('#sharedWithMeListsSection').hide();
		$('#sharedWith').hide();
		$('#shareWithForm').show();
		$('#one-list-section').show();
		$('#one-list-section h2').text(todolistName);
	}
	$('#todo-items-unchecked').html("");
	$('#todo-items-checked').html("");
}

function create_new_todo_item() {
	$('#newTodoItemSpinner').show();
	$.ajax({
		url : current_todo_list_uri + '/items',
		method : 'POST',
		success : openTodoListRequest,
		error : on_error,
		data : {
			text : $('#new-todo-item-text').val()
		},
	});
//	$('#new-todo-item-text').val('');
	return false;
}

function removeItem() {
	var current_todo_item_uri = $(this).attr('data-uri');
	$.ajax({
		url : current_todo_item_uri + "/remove",
		method : 'POST',
		success : openTodoListRequest,
		error : on_error,
	});
	return false;
}

function toggle_check() {
	var current_todo_item_uri = $(this).attr('data-uri');
	var value = false;
	if ($(this).attr('checked') == undefined) {
		value = true;
	}
	$(this).prop('checked',value);
	$(this).hide(300);
	$(this).nextAll().hide(300);
	$.ajax({
		url : current_todo_item_uri + "/check",
		method : 'POST',
		success : openTodoListRequest,
		error : on_error,
		data : {
			checked : value,
		},
	});
	return false;
}

function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}

function validateEmail(email) {
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	return re.test(email);
}

function toggleNewListForm() {
	$('#newListOptions').toggle(250);
	$('#new-list-form').toggle(500);
}



function startTodoListLoading() {
	$('#uncheckedSpinner').show();
	$('#checkedSpinner').show();
}
function stopTodoListLoading() {
	stopSharedWithMeLoading()
	stopShareListLoading()
	$('#uncheckedSpinner').hide();
	$('#checkedSpinner').hide();
	$('#newTodoItemSpinner').hide();

}
function loginAnimation(){
	// Switches the forms
	$('.loginRegister').animate({
		height : "toggle",
		'padding-top' : 'toggle',
		'padding-bottom' : 'toggle',
		opacity : "toggle"
	}, "slow");
}


function toggleShareWith() {
	$('#sharedWith').toggle(200);
}

function toggleEditListMode(){
	$('.deleteList').toggle(200);
}
function toggleEditItemMode() {
	$('.removeItem').toggle(200)
}
function startMyListsLoading() {
	$('#my-lists-spinner').show();
}

function stopMyListsLoading() {
	$('#my-lists-spinner').hide();

}
function startSharedWithMeLoading() {
	$('#sharedWithMeSpinner').show();
}

function stopSharedWithMeLoading() {
	$('#sharedWithMeSpinner').hide();
}
function startShareListLoading() {
	$('#sharedWithSpinner').show();
}
function stopShareListLoading() {
	$('#sharedWithSpinner').hide();
}
