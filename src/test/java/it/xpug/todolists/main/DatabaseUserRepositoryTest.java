package it.xpug.todolists.main;

import static org.junit.Assert.*;

import it.xpug.todolists.repository.DatabaseUserRepository;
import it.xpug.toolkit.db.*;

import org.junit.*;

public class DatabaseUserRepositoryTest {

	public static final String TEST_DATABASE_URL = "postgres://todolists:secret@localhost:5432/todolists_test";
	private DatabaseConfiguration configuration = new DatabaseConfiguration(TEST_DATABASE_URL);
	private Database database = new Database(configuration);
	DatabaseUserRepository repository = new DatabaseUserRepository(database);

	@Before
	public void clear() {
		repository.clear();
	}
	@Test
    public void saveAUser() throws Exception {
		int newUserId = repository.add(new User(1,"name", "email"), "password");

		User found = repository.get(newUserId);
		assertEquals("name", found.getField(User.UserFields.NAME));
		assertEquals("email", found.getField(User.UserFields.EMAIL));
    }

	@Test
    public void authenticateAUser() throws Exception {
		repository.add(new User(2,"name", "email"), "password");

		User found = repository.authenticate("email", "password");
		assertEquals("name", found.getField(User.UserFields.NAME));
		assertEquals("email", found.getField(User.UserFields.EMAIL));

		assertNull("wrong password", repository.authenticate("email", "BAD PASSWORD"));
		assertNull("wrong email", repository.authenticate("WRONG EMAIL", "password"));
    }

}
