package it.xpug.todolists.main;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import it.xpug.todolists.repository.DatabaseShareTodolistRepository;
import it.xpug.todolists.repository.DatabaseTodoListRepository;
import it.xpug.todolists.repository.DatabaseUserRepository;
import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.DatabaseConfiguration;
import it.xpug.toolkit.db.DatabaseTest;

public class DatabaseShareRepoTest {

	private DatabaseConfiguration configuration = new DatabaseConfiguration(DatabaseTest.TEST_DATABASE_URL);
	private Database database = new Database(configuration);
	private DatabaseUserRepository userRepository=new DatabaseUserRepository(database);
	private DatabaseTodoListRepository listRepository = new DatabaseTodoListRepository(
			database, userRepository );
	private DatabaseShareTodolistRepository share=new DatabaseShareTodolistRepository(database, listRepository);
	
	
	@Before
	public void clearRepository() {
		listRepository.clear();
		userRepository.clear();
		share.clear();
	}
	@Test
	public void shareTest(){
		User us = addAUserOnRepo();
		User us2 = addAUserOnRepo();

		String name = "prova";
		TodoList todoList = new TodoList(0, us, name);
		todoList.setId(listRepository.add(todoList));
		share.shareList(todoList, us2);

		assertEquals("repo now contains 1", 1, share.size());
	}

	
	

	private int emailNumber=0;
	
	private User addAUserOnRepo() {
		User owner = new User(0, "name", "email"+emailNumber++);
	    owner.setId(userRepository.add(owner, "secret"));
		return owner;
	}

}
