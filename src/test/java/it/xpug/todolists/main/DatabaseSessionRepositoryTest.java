package it.xpug.todolists.main;

import static org.junit.Assert.assertEquals;

import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import it.xpug.todolists.repository.DatabaseSessionRepository;
import it.xpug.todolists.repository.DatabaseUserRepository;
import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.DatabaseConfiguration;

public class DatabaseSessionRepositoryTest {
	public static final String TEST_DATABASE_URL = "postgres://todolists:secret@localhost:5432/todolists_test";
	private DatabaseConfiguration configuration = new DatabaseConfiguration(TEST_DATABASE_URL);
	private Database database = new Database(configuration);
	DatabaseUserRepository users = new DatabaseUserRepository(database);
	DatabaseSessionRepository sessions = new DatabaseSessionRepository(database, users);
	User test=new User("test", "test@test.test");

	
	@Before
	public void clear() {
		users.clear();
		sessions.clear();
		test.setId(users.add(test, "test"));		
	}
	
	@Test
	public void testAdd() {
		String id = "myID";
		String ip = "ip";
		GregorianCalendar now = new GregorianCalendar();
		sessions.add(new TodoListSession(id, test, ip,now,now));
		assertEquals("repo now contains 1", 1, sessions.size());
		TodoListSession todoListSessionFound = sessions.get(id);
		assertEquals(id, todoListSessionFound.getId());
		assertEquals(test.toString(), todoListSessionFound.getUser().toString());
		assertEquals(ip, todoListSessionFound.getIpAddress());
		assertEquals(now, todoListSessionFound.getCreationDate());
		assertEquals(now, todoListSessionFound.getUpdateDate());



	}

}
