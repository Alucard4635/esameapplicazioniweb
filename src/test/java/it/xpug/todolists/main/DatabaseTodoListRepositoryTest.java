package it.xpug.todolists.main;

import static org.junit.Assert.*;

import java.util.*;

import it.xpug.todolists.repository.DatabaseTodoListRepository;
import it.xpug.todolists.repository.DatabaseUserRepository;
import it.xpug.toolkit.db.*;

import org.junit.*;

public class DatabaseTodoListRepositoryTest {
	private DatabaseConfiguration configuration = new DatabaseConfiguration(DatabaseTest.TEST_DATABASE_URL);
	private Database database = new Database(configuration);
	private DatabaseUserRepository userRepository=new DatabaseUserRepository(database);
	private DatabaseTodoListRepository listRepository = new DatabaseTodoListRepository(
			database, userRepository );

	@Before
	public void clearRepository() {
		listRepository.clear();
		userRepository.clear();
	}

	@Test
    public void initiallyEmpty() throws Exception {
		assertEquals("repo initially empty", 0, listRepository.size());
    }

	@Test
    public void insertOneElement() throws Exception {
	    TodoList todoList = new TodoList(0, addAUserOnRepo(), "prova");
		int newId = listRepository.add(todoList);

		assertEquals("repo now contains 1", 1, listRepository.size());
		assertEquals(newId, todoList.getId());
    }

	private User addAUserOnRepo() {
		User owner = new User(0, "name", "email");
	    owner.setId(userRepository.add(owner, "secret"));
		return owner;
	}

	@Test
    public void getOneElement() throws Exception {
	    User owner = addAUserOnRepo();
		TodoList todoList = new TodoList(1, owner, "prova");
		int id = listRepository.add(todoList);

		TodoList todoListFromRepo = listRepository.get(id);

		assertEquals("prova", todoListFromRepo.getName());
		assertEquals(id, todoListFromRepo.getId());
    }

	@Test
    public void notFound() throws Exception {
	    assertNull("not found", listRepository.get(9786987));
    }

	@Test
	public void allLists() throws Exception {
	    User owner = addAUserOnRepo();
		int id0 = listRepository.add(new TodoList(0, owner, "zero"));
	    int id1 = listRepository.add(new TodoList(1, owner, "one"));

	    List<? extends TodoList> allLists = listRepository.getAllOwned(owner.getId());

	    assertEquals(2, allLists.size());
	    assertEquals("zero", allLists.get(0).getName());
	    assertEquals(id0, allLists.get(0).getId());
	    assertEquals("one", allLists.get(1).getName());
	    assertEquals(id1, allLists.get(1).getId());
	}

	@Test
    public void updateListWithNewTodoItem() throws Exception {
		TodoList todoList = new TodoList(0, addAUserOnRepo(), "zero");
		int todoListId = listRepository.add(todoList);
		todoList.setId(todoListId);

		TodoItem todoItem = new TodoItem("pippo");
		todoItem.setChecked(true);
		todoList.addItem(todoItem);

		listRepository.update(todoList);

		TodoList foundList = listRepository.get(todoListId);
		List<TodoItem> todoItems = foundList.getItems();
		assertEquals(1, todoItems.size());
		assertEquals("pippo", todoItems.get(0).getText());
		assertEquals(true, todoItems.get(0).isChecked());
    }
}
