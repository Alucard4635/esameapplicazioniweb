package it.xpug.todolists.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import it.xpug.todolists.main.SharedPrivileges;
import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.User;

public class InMemoryShareTodolistRepository implements ShareTodolistRepository {
	private ConcurrentHashMap<Integer, SharedTodolist> relation=new ConcurrentHashMap<Integer,SharedTodolist>();

	@Override
	public int shareList(TodoList todoL, User target) {
		addRelation(todoL, target, false,false);
		return relation.size();
	}

	private void addRelation(TodoList todoL, User target, boolean canCheck, boolean canCreateItems) {
		int id = todoL.getId();
		SharedTodolist sharedTodolist = relation.get(id);
		if (sharedTodolist==null) {
			sharedTodolist=new SharedTodolist(todoL);
			relation.put(new Integer(id),sharedTodolist);
		}
		Boolean[] privileges={canCheck,canCreateItems};
		sharedTodolist.usersPrivilegies.put(target,privileges );
	}

	public int shareList(TodoList todoL, User target, boolean canCheck, boolean canCreateItems) {

		addRelation(todoL, target, canCheck,canCreateItems);
		return relation.size();
	}

	@Override
	public List<TodoList> getAllSharedWith(int targetID) {
		List<TodoList> result=new LinkedList<TodoList>();
		Collection<SharedTodolist> values = relation.values();
		for (SharedTodolist sharedTodolist : values) {
			if (sharedTodolist.usersPrivilegies.containsKey(targetID)) {
				result.add(sharedTodolist.list);
			}
		}
		
		return result;
	}

	public List<User> getSharePrivileges(int todolistID) {
		List<User> resullt=new LinkedList<User>();
		Enumeration<User> keys = relation.get(todolistID).usersPrivilegies.keys();
		while(keys.hasMoreElements()){
			resullt.add(keys.nextElement());
		}
		return resullt;
	}
	
	private class SharedTodolist {
		ConcurrentHashMap<User, Boolean[]> usersPrivilegies=new ConcurrentHashMap<User,Boolean[]>();
		TodoList list;

		public SharedTodolist(TodoList l) {
			list = l;
		}
		
	}

	@Override
	public long size() {
		return relation.size();
	}

	@Override
	public void clear() {
		relation.clear();
	}

	@Override
	public List<SharedPrivileges<Boolean>> getSharedPrivileges(int todolistID) {
		// TODO Auto-generated method stub
		ArrayList<SharedPrivileges<Boolean>> arrayList = new ArrayList<>();
		arrayList.add(new SharedPrivileges<Boolean>(new User(0, "name", "email"),true,true,true,true));
		return arrayList;
	}

	@Override
	public void updatePrivileges(TodoList todolistWithPrivileges) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePrivileges(int idList,
			SharedPrivileges<Boolean> sharedPrivileges) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void revokeShare(TodoList shared, User target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isSharedTo(TodoList todoList, User target) {
		// TODO Auto-generated method stub
		return false;
	}

}
