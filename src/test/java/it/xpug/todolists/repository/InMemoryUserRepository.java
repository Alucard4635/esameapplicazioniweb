package it.xpug.todolists.repository;

import java.util.*;

import it.xpug.todolists.main.User;
import it.xpug.todolists.main.User.UserFields;

public class InMemoryUserRepository implements UserRepository {

	Map<String, User> usersByPassword = new HashMap<String, User>();
	@Override
	public int add(User user,String clearTextPassword) {//password is used for store, after encrypt it
		usersByPassword.put(clearTextPassword, user);
		return usersByPassword.size();
    }
	@Override
	public User authenticate(String email, String clearTextPassword) {
	    User user = usersByPassword.get(clearTextPassword);
	    if (null == user){
	    	return null;
	    	}
	    if (!user.getField(UserFields.EMAIL).equals(email)){
	    	return null;
	    	}
	    return user;
    }
	@Override
	public User get(int newUserId) {
		// TODO Auto-generated method stub
		Set<String> keySet = usersByPassword.keySet();
		int i=0;
		for (String email : keySet) {
			i++;
			if (i==newUserId) {
				return usersByPassword.get(email);
			}
		}
		return null;
	}
	@Override
	public void clear() {
		usersByPassword.clear();
	}
	@Override
	public User get(String identifier, UserIdentifierType identifierType) {
		// TODO Auto-generated method stub
		return null;
	}


}
