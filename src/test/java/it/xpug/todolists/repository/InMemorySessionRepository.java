package it.xpug.todolists.repository;

import java.security.SecureRandom;
import java.util.*;

import it.xpug.todolists.main.TodoListSession;
import it.xpug.todolists.main.User;

public class InMemorySessionRepository implements SessionRepository {

	private Random random;
	private Map<String, TodoListSession> sessions = new HashMap<String, TodoListSession>();

	public InMemorySessionRepository(Random random) {
		this.random = random;
    }

	public InMemorySessionRepository() {
		this(new SecureRandom());
    }

	@Override
	public String newSessionId() {
		return Long.toHexString(Math.abs(random.nextLong()));
    }

	@Override
	public void add(TodoListSession session) {
		sessions.put(session.getId(), session);
    }

	@Override
	public TodoListSession get(String sessionId) {
	    return sessions.get(sessionId);
    }

	@Override
	public void clear(User userFound) {
		Set<String> keySet = sessions.keySet();
		TodoListSession todoListSession;
		String found = null;
		for (String key : keySet) {
			
			todoListSession = sessions.get(key);
			if (todoListSession.getUser().equals(userFound)) {
				found = key;
				break;
			}
		}
		if (found!=null) {
			sessions.remove(found);
		}
	}
}
