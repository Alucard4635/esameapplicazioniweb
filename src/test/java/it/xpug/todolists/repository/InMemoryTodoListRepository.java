package it.xpug.todolists.repository;

import static java.util.Collections.*;

import java.util.*;

import it.xpug.todolists.main.TodoList;

public class InMemoryTodoListRepository implements TodoListRepository {
	private List<TodoList> todoLists = synchronizedList(new ArrayList<TodoList>());


	@Override
	public TodoList get(int todoListId) {
		return todoLists.get(todoListId);
	}

	@Override
	public int add(TodoList todoList) {
		synchronized (todoLists) {
			todoLists.add(todoList);
			todoList.setId(todoLists.size()-1);
			return todoList.getId();
        }
	}


	@Override
    public void clear() {
		todoLists.clear();
    }

	@Override
    public long size() {
	    return todoLists.size();
    }

	@Override
    public void update(TodoList todoList) {
		int index=0;
		Iterator<TodoList> iterator = todoLists.iterator();
		for (; iterator.hasNext();) {
			TodoList todoList2 = (TodoList) iterator.next();
			if (todoList.getId()==todoList2.getId()) {
				iterator.remove();
				break;
			}
			index++;
		}
		if (iterator.hasNext()||index>=todoLists.size()) {
			todoLists.add(index,todoList);
		}
    }

	@Override
	public List<TodoList> getAllOwned(int ownerID) {
		return new ArrayList<TodoList>(todoLists);
	}

	@Override
	public void delete(TodoList todoList) {
		todoLists.remove(todoList.getId());
	}

}
